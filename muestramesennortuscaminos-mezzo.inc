\context Staff = "mezzosoprano" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Mezzosoprano"
	\set Staff.shortInstrumentName = "M."
	\set Staff.midiInstrument = "Voice Oohs"
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-mezzosoprano" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 4/4
		\clef "treble"
		\key e \minor

		R1*2  |
		b 8 b b b d' 4 b 8 d'  |
		b 4 b 2 ~ b 8 r  |
%% 5
		d' 4 d' 8 d' d' 4 b 8 d'  |
		e' 4 e' 2 r4  |
		b 8 b b b d' 4 b 8 d'  |
		b 2 ~ b 8 r b b  |
		d' 4 d' 4. d' 8 b d'  |
%% 10
		e' 2 ~ e' 8 e' d' c'  |
		d' 4 d' 4. d' 8 d' d'  |
		b 4 b 4. r8 b b  |
		d' 4 d' 4. d' 8 b d'  |
		e' 2 r8 e' d' c'  |
%% 15
		d' 4 d' 4. d' 8 d' d'  |
		b 4 b 2 r8 b  |
		b 8 b b b d' d' b d'  |
		b 4 b 2 r8 b  |
		b 4 b 8 b d' d' b d'  |
%% 20
		e' 4 e' 2 r8 b  |
		b 8 b b b d' d' b d'  |
		b 4 b 4. r8 b b  |
		d' 2 ~ d' 8 d' b d'  |
		e' 2 r8 e' d' c'  |
%% 25
		d' 2 ~ d' 8 d' d' d'  |
		b 2 ~ b 8 r b b  |
		d' 2 ~ d' 8 d' b d'  |
		e' 2 r8 e' d' c'  |
		d' 2 ~ d' 8 d' d' d'  |
%% 30
		b 2. r4  |
		R1  |
		b 8 b b b d' 4 b 8 d'  |
		b 4 b 2 ~ b 8 r  |
		b 8 b b b d' 4 b 8 d'  |
%% 35
		b 4 b 2.  |
		R1  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-mezzosoprano" {
		Mués -- tra -- me, Se -- ñor, tus ca -- mi -- nos, __
		guí -- a -- me por tus sen -- de -- ros.
		En -- ca -- mí -- na -- "me en" tu ver -- dad, __
		pues tú e -- res mi sal -- va -- dor __
		"y en" ti con -- fí -- o a to -- das ho -- ras,
		pues tú e -- res mi sal -- va -- dor
		"y en" ti con -- fí -- o a to -- das ho -- ras.

		"No ol" -- vi -- des el a -- mor y la ter -- nu -- ra
		que siem -- pre nos has ma -- ni -- fes -- ta -- do.
		Ol -- ví -- da -- te, Se -- ñor, de mis pe -- ca -- dos
		pe -- "ro a" -- cuér -- da -- te de mí
		por tu a -- mor __ y tu bon -- dad, __
		pe -- "ro a" -- cuér -- da -- te de mí
		por tu a -- mor __ y tu bon -- dad.

		Mués -- tra -- me, Se -- ñor, tus ca -- mi -- nos. __
		Mués -- tra -- me, Se -- ñor, tus ca -- mi -- nos.
	}
>>
