\context ChordNames
	\chords {
		\set majorSevenSymbol = \markup { "maj7" }
		\set chordChanges = ##t
		% intro
		e2:m d2 e1:m

		% muestrame, senor, tus caminos...
		e2:m d2 e1:m
		d1 e1:m
		e2:m d2 e1:m
		d1 c1
		d1 e1:m
		d1 c1
		d1 e1:m

		% no olvides el amor...
		e2:m d2 e1:m
		d1 e1:m
		e2:m d2 e1:m
		d1 c1
		d1 e1:m
		d1 c1
		d1 e1:m

		% finale
		e2:m d2 e2:m d2
		e2:m d2 e2:m d2
		e1:m e1:m
	}
