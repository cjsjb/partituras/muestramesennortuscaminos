\context Staff = "tenor" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Tenor"
	\set Staff.shortInstrumentName = "T."
	\set Staff.midiInstrument = "Voice Oohs"
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-tenor" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 4/4
		\clef "treble_8"
		\key e \minor

		R1*2  |
		e 8 b b b a 4 g 8 fis  |
		e 4 e 2 ~ e 8 r  |
%% 5
		fis 4 fis 8 fis fis 4 e 8 d  |
		e 4 e 2 r4  |
		e 8 b b b a 4 g 8 fis  |
		e 2 ~ e 8 r e e  |
		fis 4 fis 4. fis 8 e fis  |
%% 10
		g 2 ~ g 8 g fis e  |
		fis 4 fis 4. fis 8 e d  |
		e 4 e 4. r8 g g  |
		a 4 a 4. a 8 g a  |
		c' 2 r8 c' a g  |
%% 15
		a 4 a 4. a 8 g g  |
		g 4 g 2 r8 e  |
		e 8 b b b a a g fis  |
		e 4 e 2 r8 e  |
		fis 4 fis 8 fis fis fis e d  |
%% 20
		e 4 e 2 r8 e  |
		e 8 b b b a a g fis  |
		e 4 e 4. r8 e e  |
		fis 2 ~ fis 8 fis e fis  |
		g 2 r8 g fis e  |
%% 25
		fis 2 ~ fis 8 fis e d  |
		e 2 ~ e 8 r g g  |
		a 2 ~ a 8 a g a  |
		c' 2 r8 b a g  |
		a 2 ~ a 8 a g fis  |
%% 30
		g 2. r4  |
		e 8 b b b a 4 g 8 fis  |
		e 4 e 2 ~ e 8 r  |
		e 8 b b b a 4 g 8 fis  |
		e 4 e 2. ~  |
%% 35
		e 1  |
		R1  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-tenor" {
		Mués -- tra -- me, Se -- ñor, tus ca -- mi -- nos, __
		guí -- a -- me por tus sen -- de -- ros.
		En -- ca -- mí -- na -- "me en" tu ver -- dad, __
		pues tú e -- res mi sal -- va -- dor __
		"y en" ti con -- fí -- o a to -- das ho -- ras,
		pues tú e -- res mi sal -- va -- dor
		"y en" ti con -- fí -- o a to -- das ho -- ras.

		"No ol" -- vi -- des el a -- mor y la ter -- nu -- ra
		que siem -- pre nos has ma -- ni -- fes -- ta -- do.
		Ol -- ví -- da -- te, Se -- ñor, de mis pe -- ca -- dos
		pe -- "ro a" -- cuér -- da -- te de mí
		por tu a -- mor __ y tu bon -- dad, __
		pe -- "ro a" -- cuér -- da -- te de mí
		por tu a -- mor __ y tu bon -- dad.

		Mués -- tra -- me, Se -- ñor, tus ca -- mi -- nos. __
		Mués -- tra -- me, Se -- ñor, tus ca -- mi -- nos. __
	}
>>
